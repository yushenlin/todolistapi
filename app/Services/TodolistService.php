<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\EventRepository;
use App\User;
use App\Event;
use Illuminate\Database\Eloquent\Collection;

class TodolistService
{
    protected $userRepo;
    protected $eventRepo;

    public function __construct(UserRepository $userRepo, EventRepository $eventRepo)
    {
        $this->userRepo = $userRepo;
        $this->eventRepo = $eventRepo;
    }

    public function getEvent(int $id) : ?Event
    {
        return $this->eventRepo->getEvent($id);
    }

    public function getAllEvents() : Collection
    {
        return $this->eventRepo->getEvents();
    }

    public function getUserEvents(int $id) : ?Collection
    {
        return $this->userRepo->getUserEvents($id);
    }

    public function getUserByName(string $name) : ?User
    {
        $user = $this->userRepo->getUserByName($name);
        if (is_null($user)) {
            $user = $this->userRepo->createUSer($name);
        }
        return $user;
    }

    public function createEvent(array $data) : Event
    {
        return $this->eventRepo->createEvent($data);
    }

    public function updateEvent(int $id, array $data) : bool
    {
        return $this->eventRepo->updateEvent($id, $data);
    }

    public function deleteEvent(int $id) : bool
    {
        return $this->eventRepo->deleteEvent($id);
    }

}
