<?php

namespace App\Http\Controllers;

use App\Services\TodolistService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\JsonResponse;
use Validator;

class EventController extends Controller
{
    /**
     *
     * @var TodolistService
     */
    private $todolistService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TodolistService $todolistService)
    {
        $this->todolistService = $todolistService;
    }

    /**
     * 查看所有待辦事項清單
     *
     * @return JsonResponse
     */
    public function showAllEvents() : JsonResponse
    {
        $eventsData = Cache::remember('all_events', 86400, function() {
            return $this->todolistService->getAllEvents();
        });

        return response()->json($eventsData);
    }

    /**
     * 查看單一使用者待辦事項清單
     *
     * @param string $id
     * @return JsonResponse
     */
    public function showUserEvents(string $id) : JsonResponse
    {
        $rules = array('id' => 'exists:users,id');
        $validator = Validator::make(['id' => $id], $rules);
        if ($validator->fails()) {
            return response()->json($validator->messages());
        }
        $userEventsData = Cache::remember('user_events_' . $id,  86400, function() use ($id) {
            return $this->todolistService->getUserEvents((int)$id);
        });
        return response()->json($userEventsData);
    }

    /**
     * 新增待辦事項
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request) : JsonResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'title' => 'required',
            'content' => 'required',

        ]);

        $user = $this->todolistService->getUserByName($request->name);

        $eventData = ['user_id' => $user->id, 'title' => $request->title, 'content' => $request->content];

        $event = $this->todolistService->createEvent($eventData);

        return response()->json($event, 201);
    }

    /**
     * 編輯待辦事項
     *
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(string $id, Request $request) : JsonResponse
    {
        $rules = array('id' => 'exists:events,id');
        $validator = Validator::make(['id' => $id], $rules);
        if ($validator->fails()) {
            return response()->json($validator->messages());
        }

        $event = $this->todolistService->updateEvent((int)$id, $request->all());

        return response()->json($event, 200);
    }

    /**
     * 刪除待辦事項
     *
     * @param string $id
     * @return JsonResponse
     */
    public function delete(string $id) : JsonResponse
    {
        $rules = array('id' => 'exists:events,id');
        $validator = Validator::make(['id' => $id], $rules);
        if ($validator->fails()) {
            return response()->json($validator->messages());
        }

        $this->todolistService->deleteEvent((int)$id);

        return response()->json(['result' => 'success'], 200);
    }

    /**
     * 查看待辦事項內容
     *
     * @param string $id
     * @return JsonResponse
     */
    public function showEventContent(string $id) : JsonResponse
    {
        $rules = array('id' => 'exists:events,id');
        $validator = Validator::make(['id' => $id], $rules);
        if ($validator->fails()) {
            return response()->json($validator->messages());
        }

        $event = $this->todolistService->getEvent((int)$id);
        return response()->json(['content' => $event->content], 200);
    }
}
