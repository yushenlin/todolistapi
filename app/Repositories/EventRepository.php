<?php

namespace App\Repositories;

use App\Event;
use Illuminate\Database\Eloquent\Collection;

class EventRepository
{
    protected $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function getEvent(int $id) : ?Event
    {
        return Event::find($id);
    }

    public function getEvents() : ?Collection
    {
        return Event::all();
    }

    public function createEvent(array $data) : Event
    {
        return Event::create($data);
    }

    public function updateEvent(int $id, array $data) : bool
    {
        return Event::findOrFail($id)->update($data);
    }

    public function deleteEvent(int $id) : bool
    {
        return Event::findOrFail($id)->delete();
    }
}
