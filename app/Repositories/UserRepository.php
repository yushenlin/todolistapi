<?php

namespace App\Repositories;

use App\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUserEvents(int $id) : ?Collection
    {
        return User::find($id)->events;
    }

    public function createUser(string $name) : User
    {
        return User::create(['name' => $name]);
    }

    public function getUserByName(string $name) : ?User
    {
        return User::where('name', '=', $name)->first();
    }

}
